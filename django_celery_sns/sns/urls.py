from django.urls import path


from .views import GenerateRandomUserView


app_name = "sns"
urlpatterns = [
    path("", view=GenerateRandomUserView.as_view(), name="randome_user"),
]
