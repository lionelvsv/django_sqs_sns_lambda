from django.apps import AppConfig


class SnsConfig(AppConfig):
    name = "django_celery_sns.sns"
    verbose_name = "sns"
