import os
import typing
from django.conf import settings
# from django_celery_sns.sns.tasks import display_time
# from sns.tasks import display_time


if not settings.configured:
    # set the default Django settings module for the 'celery' program.
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings.local')  # pragma: no cover

from celery import Celery

# set the default django settings module
# os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'celerysqs.settings')

from django.conf import settings  # NOQA

app = Celery('django_celery_sns')  # type: Celery

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


app.conf.beat_schedule = {
    'display_time-30-seconds': {
        'task': 'django_celery_sns.sns.tasks.display_time',
        'schedule': 10.0
    },
}

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))